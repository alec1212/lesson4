package ru.volnenko.tm;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

@WebService
public class calculator {

    @WebMethod
    public double add ( @WebParam (name = "a") double a, @WebParam (name = "b")double b){
        return a+b;
    }

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/calculator?wsdl", new calculator());


    }
}
