package ru.volnenko.tm.repository;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RepositoryWrapper{

    @XmlElement
    @JsonProperty
    private ProjectRepository projectRepository;
    @XmlElement
    @JsonProperty
    private TaskRepository taskRepository;

    public RepositoryWrapper()
    {
        this.projectRepository = new ProjectRepository();
        this.taskRepository = new TaskRepository();

    }
    public RepositoryWrapper( ProjectRepository projectRepository, TaskRepository taskRepository)
    {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;

    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }
}
